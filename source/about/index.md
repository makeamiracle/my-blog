---
title: About Me
date: 2021-07-08 20:18:49
type: "about"
updated:
top_img: /resource/Rocket.jpg
mathjax:
katex:
aside:
aplayer:
highlight_shrink:
---

# 关于我

## 1. 我是谁

姓名：`Yu Zhen`

## 2. 接受教育

2013 ~ 2017 燕山大学 电子信息工程

2018 ~ 2021 太原理工大学 计算机科学与技术

2019 ~ 2020 美国 西佛罗里达大学  数据科学

## 3. 会些什么

### 3.1 编程语言

|  编程语言   | 掌握程度 | 代表作 |
| :---------: | :------: | :----: |
| C Language  |   熟练   |        |
|     C++     | 略懂一二 |        |
|    Java     |          |        |
|   Python    |          |        |
| ShellScript |          |        |

![MainBack](index.assets/MainBack.jpg)

